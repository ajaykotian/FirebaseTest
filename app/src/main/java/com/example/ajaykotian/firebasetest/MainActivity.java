package com.example.ajaykotian.firebasetest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private Button button;
    private Button insert;
    private Button load;
    private TextView tvDisplay;
    private SignInButton signInButton;
    private int RC_SIGN_IN = 9001;
    private DatabaseReference mDatabase;

    //private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        final GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        Fabric.with(this, new Crashlytics());
        Crashlytics.setUserIdentifier("TEST USER");

        editText = (EditText) findViewById(R.id.et);
        button = (Button) findViewById(R.id.btn_crash);
        insert = (Button) findViewById(R.id.btn_insert_db);
        load = (Button) findViewById(R.id.btn_load_db);
        tvDisplay = (TextView) findViewById(R.id.tv_display);
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        //imageView = (ImageView) findViewById(R.id.image);

        final Random r = new Random(1244);

        final ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Log.e("Class name: ",);
                Log.e("Class key: ", dataSnapshot.getKey() + "");

                List<TestStructure> data = dataSnapshot.getValue(Main.class).getTestStructures();
                String s = "";
                if (data != null) {

                    for (int i = 0; i < data.size(); i++) {
                        s += data.get(i).getFirst() + "   " + data.get(i).getSecond() + "\n\n";
                    }
                    tvDisplay.setText(s);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        database.getReference().addValueEventListener(valueEventListener);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        //Picasso.with(this).load("https://firebasestorage.googleapis.com/v0/b/fir-test-e8142.appspot.com/o/thumb-1920-36519.jpg?alt=media&token=c2362a38-736f-4fcd-8eed-345d4e698313").into(imageView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = editText.getText().toString();
                if (s.isEmpty()) {
                    s = "empty";
                }

                Crashlytics.log(s);
                throw new RuntimeException(s);
            }
        });
/*
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });*/

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                r.setSeed(r.nextInt());

                TestStructure testStructure = new TestStructure("first " + r.nextInt(), "second " + r.nextInt());

                //database.getReference().child("testStructure").setValue(testStructure);

                database.getReference().push().setValue(testStructure);

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("Google signin ", "handleSignInResult:" + result.isSuccess());
        Log.e("Google signin ", "Status code:" + result.getStatus());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Toast.makeText(this, acct.getDisplayName(), Toast.LENGTH_SHORT).show();

        } else {
            // Signed out, show unauthenticated UI.
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
        }
    }


    public static class Main {
        List<TestStructure> testStructures;

        public Main() {
        }

        public Main(List<TestStructure> testStructures) {
            this.testStructures = testStructures;
        }

        public List<TestStructure> getTestStructures() {
            return testStructures;
        }

        public void setTestStructures(List<TestStructure> testStructures) {
            this.testStructures = testStructures;
        }
    }

    public static class TestStructure {
        String first, second;

        public TestStructure() {
        }

        public TestStructure(String first, String second) {
            this.first = first;
            this.second = second;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public void setSecond(String second) {
            this.second = second;
        }

        public String getFirst() {
            return first;
        }

        public String getSecond() {
            return second;
        }
    }

}
